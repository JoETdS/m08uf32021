package dam2021.mp08.uf1.movimentsperpantalla;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, SensorEventListener {



    private float inicialX, inicialY, finalX, finalY;
    private TextView textDireccio;
    private SensorManager sensorManager;
    private Sensor accelerometre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.textDireccio = (TextView) findViewById(R.id.textDireccio);
        this.textDireccio.setText("X");

        ConstraintLayout ly = (ConstraintLayout) findViewById(R.id.layoutMain);
        ly.setOnTouchListener(this);

        this.sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.accelerometre = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //this.sensorManager.registerListener(this, this.accelerometre, SensorManager.SENSOR_DELAY_NORMAL);

}

    /*
    opcions principals de MotionEvent
        ACTION_DOWN: es produeix quan l’usuari inicia una pulsació, és a dir,
            al moment exacte que el dit fa contacte en la pantalla.
        ACTION_UP: quan finalitzem la pulsació, quan el dit deixa de tocar la pantalla.
        ACTION_MOVE: entre els events ACTION_DOWN i ACTION_UP hi ha hagut algun canvi en la posició del dit,
            si hi ha moviment és perquè l’usuari ha desplaçat el dit per la pantalla durant la pulsació.
        ACTION_POINTER_DOWN: s’ha produït una pulsació d’un dit que no és el principal (passem a generar events multi touch).
            Quan aquest dit s’aixeca de la pantalla genera un ACTION_POINTER_UP.
        ACTION_POINTER_INDEX_MASK:
            conjuntament amb ACTION_POINTER_INDEX_SHIFT ens permetrà saber quin índex té la pulsació en un event multi touch.
     */

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        String dalt = "va cap a dalt";
        String baix = "va cap a baix ";

        /*if(event.getAction() == MotionEvent.ACTION_DOWN){
            this.inicialX = event.getX();
            this.inicialY = event.getY();
        }else if(event.getAction() == MotionEvent.ACTION_UP){
            this.finalX = event.getX();
            this.finalY = event.getY();
            if(this.finalY - this.inicialY < 0 ){
                this.textDireccio.setText(dalt);
            }else{
                this.textDireccio.setText(baix);
            }/

         */

        //movem el text en funcio don esta el dit
        if(event.getAction() == MotionEvent.ACTION_MOVE){
            this.textDireccio.setX(event.getX());
            this.textDireccio.setY(event.getY());
        }

        //movem el text allí on premem
        if(event.getAction() == MotionEvent.ACTION_UP){
            this.textDireccio.setX(event.getX());
            this.textDireccio.setY(event.getY());
        }

            return true;
        }

    @Override
    protected void onPause() {
        super.onPause();
        this.sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        //this.sensorManager.registerListener(this,accelerometre, SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            float initialX = 0, initialY = 0;
            float x, y, z;

            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            if(initialX > x){
                this.textDireccio.setX(this.textDireccio.getX()+1);
            }else{
                this.textDireccio.setX(this.textDireccio.getX()-1);
            }
            if(initialY > y){
                this.textDireccio.setY(this.textDireccio.getY()+1);
            }else{
                this.textDireccio.setY(this.textDireccio.getY()-1);
            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}